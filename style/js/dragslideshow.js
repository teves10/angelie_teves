/**
 * dragslideshow.js v1.0.0
 * http://www.codrops.com
 *
 * Licensed under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 * 
 * Copyright 2014, Codrops
 * http://www.codrops.com
 */
;( function( window ) {
	
	'use strict';
	
	var docElem = window.document.documentElement,
		transEndEventNames = {
			'WebkitTransition': 'webkitTransitionEnd',
			'MozTransition': 'transitionend',
			'OTransition': 'oTransitionEnd',
			'msTransition': 'MSTransitionEnd',
			'transition': 'transitionend'
		},
		transEndEventName = transEndEventNames[ Modernizr.prefixed( 'transition' ) ],
		support = { transitions : Modernizr.csstransitions };

	/**
	 * gets the viewport width and height
	 * based on http://responsejs.com/labs/dimensions/
	 */
	function getViewport( axis ) {
		var client, inner;
		if( axis === 'x' ) {
			client = docElem['clientWidth'];
			inner = window['innerWidth'];
		}
		else if( axis === 'y' ) {
			client = docElem['clientHeight'];
			inner = window['innerHeight'];
		}
		
		return client < inner ? inner : client;
	}

	/**
	 * extend obj function
	 */
	function extend( a, b ) {
		for( var key in b ) { 
			if( b.hasOwnProperty( key ) ) {
				a[key] = b[key];
			}
		}
		return a;
	}

	/**
	 * DragSlideshow function
	 */
	function DragSlideshow( el, options ) {	
		this.el = el;
		this.options = extend( {}, this.options );
		extend( this.options, options );
		this._init();
	}

	/**
	 * DragSlideshow options
	 */
	DragSlideshow.prototype.options = {
		perspective : '1200',
		slideshowRatio : 0.8, // between: 0,1
		onToggle : function() { return false; },
		onToggleContent : function() { return false; },
		onToggleContentComplete : function() { return false; }
	}

	/**
	 * init function
	 * initialize and cache some vars
	 */
	DragSlideshow.prototype._init = function() {
		var self = this;
        
		// current
        this.currentSlug = $("#currentSlug").val();
        this.scrollingTop = false;
        this.primoAccessoHome = false;
        this.breakpoint = "mobile";
        this.menuOffset = 30;
        this.lePages;
        this.loadingSlug = "";
        this.autoscrollTo = false;
        
        
        if(this.currentSlug!="home"){
            $(".div-arrow").addClass("bounce");        
        } else {
            $("#bgvid").css("opacity",0);
            $(".slider-switch").css("opacity",0);
            $("#logo_h").css("opacity",0);
            $("#titolo_h").css("opacity",0);            
            $("#pulsantozzo_home").css("opacity",0);
        }
        
        // inizializza il point menu
        for(var x=0;x<$(".slide").length;x++) {
            $("#pointMenu").append("<div data-num='"+x+"' class='point'></div>");
        }
        $("#pointMenu .point[data-num='0']").addClass("active");
        
		this.current = $(".slide-"+this.currentSlug).data("index");
        
        if(this.currentSlug=="portfolio") this.current=1;
        
        this.autoOpen = false;
        this.totalPages = $(".slide").length-1;
        $(".slider-handle").css("width",(100/this.totalPages)+"%");

		// status
		this.isFullscreen = true;
		
		// the images wrapper element
		this.imgDragger = this.el.querySelector( 'section.dragdealer' );
		
		// the moving element inside the images wrapper
		this.handle = this.imgDragger.querySelector( 'div.handle' );
		
		// the slides
		this.slides = [].slice.call( this.handle.children );
		
		// total number of slides
		this.slidesCount = this.slides.length;
		
		if( this.slidesCount < 1 ) return;

		// cache options slideshowRatio (needed for window resize)
		this.slideshowRatio = this.options.slideshowRatio;

		// add class "current" to first slide
		classie.add( this.slides[ this.current ], 'current' );
		
		// the pages/content
		this.pages = this.el.querySelector( 'section.pages' );
		
		// initialize the DragDealer plugin
		this._initDragDealer();

		// init events
		this._initEvents();
        $(".slide-"+this.currentSlug+" .img_blurred").css('opacity',1);
        
        $("#numSlider").change(function(){
            if($(this).val()<=0){
                $(".left-section a").css("color","#fff");
                $(".slider-handle").css("opacity",0);
            } else if($(this).val()>=self.totalPages){
                $(".right-section a").css("color","#fff");
                $(".slider-handle").css("opacity",0);
            } else {
                $(".right-section a").css("color","#505050");
                $(".left-section a").css("color","#505050");
                $(".slider-handle").css("opacity",1);
            }
        });
        
        if(this.currentSlug=="contatti"){
            $(".mn3").trigger("click");
        }
        if(this.currentSlug=="portfolio"){
            $("#mn-portfolio a").trigger("click");
            $("#container").css("background-color",$(".slide[data-index=1]").attr("data-colore"));
            
            $("#pointMenu .point").removeClass("active");
            $("#pointMenu .point[data-num='1']").addClass("active");
            
        }        
        
        $(window).bind("load", function() {
            //if(navigator.platform == 'iPad' || navigator.platform == 'iPhone' || navigator.platform == 'iPod'){
            //    $(".js .dragslider").css("position", "fixed").css("top", $('window').height());
            //};

            /*
            self.imgDragger.style.WebkitTransform = 'perspective(' + self.options.perspective + 'px) translate3d( 0, 50%, -300px )';
            self.imgDragger.style.transform = 'perspective(' + self.options.perspective + 'px) translate3d( 0, 50%, -300px )';    
            $("#header").removeClass("outed");
            $("body").removeClass("full");
            $("body").removeClass("allFull");
            */
            
            if($("#body").hasClass("m") && !($("#body").hasClass("ios"))){
                $(".js .dragslider").css("position", "fixed").css("top", $('window').height());
                revealLoad();
            } else {
                //document.getElementById("bgvid").oncanplaythrough = function() {
                if(this.currentSlug=="home") {
                    document.getElementById("bgvid").oncanplay = function() {
                        if(!$("body").hasClass("fullyLoaded"))
                            revealLoad();
                    };
                } else {
                    if(!$("body").hasClass("fullyLoaded"))
                            revealLoad();
                    }
                }
        });
        
        function revealLoad(){
            $("body").addClass("fullyLoaded");
            //self.toggle(); // per partire con il menu aperto.
            $("#preloader").fadeOut("slow",function(){
                $(this).remove()
            });
            self._revealContent(self.currentSlug);  
        }
        
        
        
	}

    DragSlideshow.prototype._initTitlesPosition = function() {
        
        var hHeader = $("#header").outerHeight(); //altezza header
        var h = (($(window).height()*0.25)+40)-(hHeader/2);
        h = h+30; // altezza del menu a pallini
        //$("#slider").css("top",(($(window).height()/2)-30)+"px");
        
        $(".slide .title").each(function(){
            $(this).css("top","-"+((h+($(this).outerHeight()/2))+"px"));
        });
        
        $(".slide .title-contatti").css("top","-"+(($(".slide .title-contatti").outerHeight()/2)+120)+"px");
    }
    
    DragSlideshow.prototype._loadContentPage = function() {
            this.primoAccessoHome = true;
            //qui
            $(".pages .content").css("display","none");
            var self = this;
            
            self.loadingSlug = self.currentSlug;
        
            if(!$("#content-"+this.currentSlug).hasClass("loaded")) {
                // non ho la pagina in cache. La carico via ajax.
                $("#progressBar").css("width","100%");
                $("#progressBar").css("height","0");
                $("#progressBar").css("marginLeft","0");
                //$("#progressBar").css("height","10px");
                var canMove = false;
                $("#progressBar").velocity({
                            height: "10px"
                        },1000,function(){canMove = true;});
                
                $.ajax({
                    xhr: function()
                      {
                        var xhr = new window.XMLHttpRequest();
                        //Download progress
                        xhr.addEventListener("progress", function(evt){
                          if (evt.lengthComputable) {
                            var percentComplete = evt.loaded / evt.total;
                            //Do something with download progress
                              if(canMove){
                                $("#progressBar").velocity({
                                marginLeft: ((percentComplete*100))+"%"
                                },300);
                              }
                          }
                        }, false);
                        return xhr;
                      },                    
                    type: 'POST',
                    url: $("#homeUrl").val().replace("it","")+"/wp-admin/admin-ajax.php",
                    dataType: 'json',
                    data: {"action": "load-content", slug: this.currentSlug },
                    success: function(response, textStats, jqXHR) {
                        //if(self.currentSlug == response.slug) {
                            $("#content-"+response.slug+" .content-page").html(response.html);
                        //}
                        /*
                        $("#content-"+self.currentSlug+" .content-page").waitForImages(function() {
                            // All descendant images have loaded, now slide up.
                            self._revealContent();
                        });
                        return;
                        */
                        //$("#progressBar").css("height","0px");
                        $("#progressBar").velocity({
                            marginLeft: "100%"
                        },200,function(){
                            //$("#progressBar").velocity({height:"0px"},200,function(){$("#progressBar").css("marginLeft","100%")});
                            $(".div-arrow").addClass("bounce");
                            //self._revealContent();    
                        });
                        
                        return false;
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log(textStatus);
                    }
                });
            } else {
                $(".div-arrow").addClass("bounce");
                //this._revealContent();
            }
        
            this._revealContent();
    }    

    DragSlideshow.prototype._revealContent = function() {
        $(".content-project img").each(function(){
            $(this).removeClass("fade-in");
            $(this).css("opacity",0);
        });
        
        //$(".content").fitVids();
        
        if(!this.primoAccessoHome && this.currentSlug=="home"){

            self = this;
            //qui
            $("#content-"+self.currentSlug).css("visibility","visible");
            $("#content-"+self.currentSlug).css("display","block");
            $("#content-"+self.currentSlug).addClass("loaded");
            
            //document.getElementById("bgvid").addEventListener('loadeddata', function() {
            //}, false);
            
            if($("#body").hasClass("d") || $("#body").hasClass("ios")){
                $("#bgvid").velocity({
                    opacity: 1
                },{
                    duration:2000,
                    //delay:500,
                    complete:function(){

                        //$("#titolo_h").velocity("transition.slideUpIn",1500);
                        $("#titolo_h").velocity("transition.slideUpIn",1500,function(){
                            $("#header").addClass("toSlide");
                            $(".div-arrow").addClass("bounce");
                            $(".slider-switch").velocity({opacity:1});
                            $("#pulsantozzo_home").velocity({opacity:1});
                        });
                    }
                }

                );
            } else {
                $("#titolo_h").velocity("transition.slideUpIn",1500);
                $("#logo_h").velocity("transition.slideDownIn",1500,function(){
                    $(".div-arrow").addClass("bounce");
                    $(".slider-switch").velocity({opacity:1});
                    $("#header").addClass("toSlide");
                    $("#pulsantozzo_home").velocity({opacity:1});
                });
            }
            
            this.primoAccessoHome = true;
        } else {
            //qui
            $("#content-"+this.currentSlug).css("visibility","visible");
            $("#content-"+this.currentSlug).css("display","block");
            $("#content-"+this.currentSlug).addClass("loaded");
            $("#header").addClass("toSlide");
            if(this.currentSlug=="portfolio"){
                this.currentSlug = $(".slide[data-index=1]").attr("data-slug");
            }
        }
        
        try{
            Flickity.data(".main-gallery").resize();
            $(".main-gallery").each(function(){
                Flickity.data("#"+$(this).attr("id")).resize();    
            });
            
        } catch(err){
            
        }
    }    
    
    
	/**
	 * initialize the events
	 */
	DragSlideshow.prototype._initEvents = function() {
		var self = this;
		
        if ($("#breakpoint").css("color") == "rgb(255, 255, 255)") {
            self.breakpoint = "desktop";
            self.menuOffset = 60;
        }
        
        $("#pointMenu .point").click(function(){
            if( self.isFullscreen || self.isAnimating ) return false;
            var pg = $(this).attr("data-num");
            $(".slide").removeClass("current");
                    
            self.current = pg;
            $(".slide[data-index="+self.current+"]").addClass("current");
            $("#container").css("background-color",$(".slide[data-index="+self.current+"]").attr("data-colore"));
            
            $(".handle").dragend({
                scrollToPage: (parseInt(pg)+1)
            });
            $("#pointMenu .point").removeClass("active");
            $("#pointMenu .point[data-num='"+pg+"']").addClass("active");
        });
        
        
        $("#mn-portfolio a,#hR,#lnkPortfolio a").click(function(e){
            e.preventDefault();
            
            var act = self.currentSlug;
            if (self.currentSlug=="contatti") self.hideContatti();
            if(!$("#body").hasClass("full")) {
                if(act=="home"){
                    if( self.isFullscreen || self.isAnimating ) return false;
                    var pg = 1;
                    $(".slide").removeClass("current");

                    self.current = pg;
                    $(".slide[data-index="+self.current+"]").addClass("current");

                    $(".handle").dragend({
                        scrollToPage: (parseInt(pg)+1)
                    });
                    $("#pointMenu .point").removeClass("active");
                    $("#pointMenu .point[data-num='"+pg+"']").addClass("active");
                } else if(act=="contatti"){
                    if( self.isFullscreen || self.isAnimating ) return false;
                    var pg = 1;
                    $(".slide").removeClass("current");

                    self.current = pg;
                    $(".slide[data-index="+self.current+"]").addClass("current");

                    $(".handle").dragend({
                        scrollToPage: (parseInt(pg)+1)
                    });
                    $("#pointMenu .point").removeClass("active");
                    $("#pointMenu .point[data-num='"+pg+"']").addClass("active");                    
                }
            } else {
                if(act=="home") {
                    self.toggle(true,1);
                } else {
                    self.toggle();
                    history.pushState(null,self.currentSlug,$("#homeUrl").val() + "/portfolio");
                }
            }
            
        });
        
        $(".icon-Freccetta-salterina,#hD").click(function(e){
            e.preventDefault();
            var altezza = ($("body").outerHeight() - 80);
            $("html,body").animate({scrollTop: altezza + "px"},1000,"easeInOutExpo");
        });
        
        
       $(".form-contatti a").click(function(){
            location.href=$(this).attr("href");
        });
        
        $(".slide").click(function(){
            
            if( self.isFullscreen || self.isAnimating ) return false;
            
            //if(self.currentSlug=="contatti") return false;
			
            if (self.currentSlug=="contatti"){
                if( !$(this).hasClass("current") ){
                    self.hideContatti();
                } else {
                    return false;
                }
            }
            
            if( $(this).hasClass("current") ) {
                    
					self.toggle();
                    history.pushState(null,self.currentSlug,$("#homeUrl").val() + "/" + self.currentSlug);
				}
				else {
					var pg = $(this).data("index");
                    $("#container").css("background-color",$(this).attr("data-colore"));
                    $(".slide").removeClass("current");
                    $("#pointMenu .point").removeClass("active");
                    self.current = pg;
                    $(this).addClass("current");
                    $(".handle").dragend({
                        scrollToPage: pg+1
                    });
                    
                    //$("#pointMenu .point[data-num='"+pg+"']").addClass("active");
                    //$("#numSlider").val(pg).change();
				}
        });
/*        
        $("#link-to-home").click(function(e){
            e.stopImmediatePropagation();
            e.preventDefault();
            $("#pointMenu .point").removeClass("active");
            $(".slide").removeClass("current");
            self.autoOpen = true;
            self.current = 0;
            $(".slide-home").addClass("current");
            $(".handle").dragend({
                scrollToPage: 1
            });
            $("#numSlider").val(0).change();
            //self.toggle();

        });
*/        
        DragSlideshow.prototype.showContatti = function() {
            $("#pointMenu").css("opacity",0);
            $("#pointMenu").css("visibility","hidden");
            $("#footer-contatti").addClass("in");
            $("#mappaContatti").addClass("in");
            history.pushState(null,self.currentSlug,$("#homeUrl").val() + "/contatti");
        }
        
        DragSlideshow.prototype.hideContatti = function() {
            $("#pointMenu").css("opacity",1);
            $("#pointMenu").css("visibility","visible");
            $("#mappaContatti").removeClass("in");
            $("#footer-contatti").removeClass("in");
            
            history.pushState(null,self.currentSlug,$("#homeUrl").val() + "/portfolio");
        }        
        
        $("body").on("click",".link-to-prj",function(e){
            e.stopImmediatePropagation();
            e.preventDefault();
            var slug = $(this).attr("data-to-slug");

            if(slug=="contatti"){
                $("#link-to-contact").trigger("click");
                return false;
            }
            
            self.autoOpen = true;
            $(".slide").each(function(){
                if (slug==$(this).data("slug")) {
                    var pg = $(this).data("index");
                    self.toggle(true,pg);
                }
            });
            
            
        });

        
window.addEventListener('popstate', function(event) {

    //location.href = window.location;
    //return;
    
    var segment = window.location.pathname.split("/").pop();
    if (segment=="") {
        return;
    }
    
    switch (segment) {
            
        case "contatti":
                $("#link-to-contact").trigger("click");
                break;
        case "home":
            $("#link-to-home").trigger("click");
            break;
        case "portfolio":
                $("#mn-portfolio a").trigger("click");
            break;
        default:
            location.href = window.location;
            break;
            
    }
    
});        
        
        $("body").on("click","#link-to-home,.mn1",function(e){
            e.stopImmediatePropagation();
            e.preventDefault();
            
            if(self.currentSlug=="home"){
                $("html,body").animate({scrollTop: 0},300,function(){

                });
                
                return false;
            }
            
            if (self.currentSlug=="contatti") self.hideContatti();
            var slug = "home";

  
            self.autoOpen = true;
            if($("#body").hasClass("full")){
                self.toggle(true,0);
            } else {
                self.autoscrollTo = true;
                self.current = 0;
                $(".slide").removeClass("current");
                $(".slide-home").addClass("current");

                $("#pointMenu .point").removeClass("active");
                $("#pointMenu .point[data-num='0']").addClass("active");
                self.autoOpen = true;
                $("#container").css("background-color",$(".slide-home").attr("data-colore"));                
                self.currentSlug = slug;
                $(".handle").dragend({
                    scrollToPage: 1
                });                                                
            }
        });        
        
        
        $("body").on("click",".mn3,#link-to-contact,.__button-sublimio-contatti",function(e){
            e.stopImmediatePropagation();
            e.preventDefault();
            var slug = "contatti";
            var maxPages = ($(".slide").length)-1;
            if($("#body").hasClass("full")){
                self.toggle(true,maxPages);
            } else {
                self.current = maxPages;
                $(".slide").removeClass("current");
                $(".slide-contatti").addClass("current");

                $("#pointMenu .point").removeClass("active");
                $("#pointMenu .point[data-num='"+maxPages+"']").addClass("active");
                //self.autoOpen = true;
                $("#container").css("background-color",$(".slide-home").attr("data-colore"));                
                self.currentSlug = slug;
                $(".handle").dragend({
                    scrollToPage: (maxPages+1)
                });                                                
            }
        });         
        
        var fadeStart=0,
            fadeStart2=0,
            buttonStart=400,
            buttonUntil = 200,
            fadeUntil=400,
            fadeUntil2=50,
            offset=0,
            opacity=0,
            opacity2=1,
            lastOffset=0,
            currentScroll=0,
            direction=1;

        $(window).bind('scroll', function(){
            
            if(self.scrollingTop)
                return;
            
            offset = $(document).scrollTop();
            opacity=0;
            opacity2=1;
            
            
            
            if( offset<=fadeStart ){
                opacity=1;
                $(".div-arrow").addClass("bounce");
            }else if( offset<=fadeUntil ){
                opacity=1-offset/fadeUntil;
                $(".div-arrow").removeClass("bounce");
            }
            
            /*
            if(offset>=lastOffset)
                direction = -1;
            else
                direction = 1;
            
            if(offset>=buttonStart && (!$(".slider-switch").is(".velocity-animating"))){
                if(offset>=buttonStart && direction==1 && $(".slider-switch").position().top==-60 ){
                    $(".slider-switch").velocity({top:[self.menuOffset+"px","ease"]},300);
                } else if(offset>=buttonStart && direction==-1 && $(".slider-switch").position().top==self.menuOffset ){
                    $(".slider-switch").velocity({top:["-60px","ease"]},300);
                }
            }
            
            
            if($(window).scrollTop() + $(window).height() == $(document).height())
            {
                $(".slider-switch").velocity({top:[self.menuOffset + "px","ease"]},300);
            }
            */
            
            if( offset<=fadeStart2 ){
                opacity2=0
            }else if( offset<=fadeUntil2 ){
                opacity2=0+offset/fadeUntil2;
            }

            if(!$(body).hasClass("full")) {
                offset=fadeUntil2+10;
                opacity=0;
                opacity2=0;
                //alert("e");
            }            
            
            
            if (opacity<=0)
                $("#content-"+self.currentSlug+" .img_blurred").css('display',"none");
            else
                $("#content-"+self.currentSlug+" .img_blurred").css('display',"inline-block");
            
            $("#content-"+self.currentSlug+" .page-intro").css('opacity',opacity);
            $(".slide-"+self.currentSlug+" .img_blurred").css('opacity',opacity);
            $("#content-"+self.currentSlug+" .section1").css('opacity',opacity2);
            
            //alert($(".slide-"+self.currentSlug+" .img_blurred").css('opacity'));
            
            if((offset-200)>=window['innerHeight'] && self.current==0){
                $("#bgvid").trigger('pause');
                $("#bgvid").css("display",'none');
            } else {
                $("#bgvid").trigger('play');
                $("#bgvid").css("display",'block');                
            }
            
            lastOffset = offset;
            
            
            $(".content-project img").each(function(){
                var thisPos = $(this).offset().top;
                if ($(window).scrollTop() + $(window).height() - 50 > thisPos ) {
                    if(!$(this).hasClass("fade-in")){
                        $(this).velocity("transition.slideUpIn");
                        $(this).addClass("fade-in");
                    }
                }
            });
            
        });
                
        window.addEventListener('resize',function(){
            self._initTitlesPosition()
        });
        
        
        window.addEventListener('orientationchange', function () {
             //if(window.innerHeight>window.innerWidth) {
                document.getElementById("container").style.display='none';
                setTimeout(function () {
                    document.getElementById("container").style.display = '';
                    $(window).resize();
                    self._initTitlesPosition()
                }, 500);
             //}
        });
        
        
        
		// keyboard navigation events
		document.addEventListener( 'keydown', function( ev ) {
            if( $("#body").hasClass("full") ) return;
			var keyCode = ev.keyCode || ev.which,
				currentSlide = self.slides[ self.current ];


                if( self.isAnimating ) return;
                
				switch (keyCode) {
					// down key
					case 40:
						// if not fullscreen don't reveal the content. If you want to navigate directly to the content then remove this check.
						if( $("#body").hasClass("full") ) return;
						//self._toggleContent( currentSlide );
						break;
					// right and left keys
					case 37:
                        
                        if(self.current==0) return;
                        self.isAnimating = true;
						$(".handle").dragend("right");
                        $(".slide").removeClass('current');
                        self.current--;
                        $(".slide[data-index="+self.current+"]").addClass("current");
                        $("#pointMenu .point[data-num='"+self.current+"']").addClass("active");
                        //$("#numSlider").val(self.current).change();
                        $("#container").css("background-color",$(".slide[data-index="+self.current+"]").attr("data-colore"));
						break;
					case 39:
                        if((self.current)==self.totalPages) return;
                        self.isAnimating = true;
						$(".handle").dragend("left");
                        $(".slide").removeClass('current');
                        self.current++;
                        $(".slide[data-index="+(self.current)+"]").addClass("current");
                        $("#pointMenu .point[data-num='"+self.current+"']").addClass("active");
                        //$("#numSlider").val(self.current).change();
                        $("#container").css("background-color",$(".slide[data-index="+self.current+"]").attr("data-colore"));
                        
						break;
				}
			//}
		} );
	}

	/**
	 * gets the content page of the current slide
	 */
	DragSlideshow.prototype._getContentPage = function( slide ) {
		return this.pages.querySelector( 'div.content[data-content = "' + slide.getAttribute( 'data-content' ) + '"]' );
	}

	/**
	 * show/hide content
	 */
    /*
	DragSlideshow.prototype._toggleContent = function( slide ) {
		if( this.isAnimating ) {
			return false;
		}
		this.isAnimating = true;

		// callback
		this.options.onToggleContent();

		// get page
		var page = this._getContentPage( slide );
		
		if( this.isContent ) {
			// enable the dragdealer
			//this.dd.enable();
			classie.remove( this.el, 'show-content' );
		}
		else {
			// before: scroll all the content up
			page.scrollTop = 0;
			// disable the dragdealer
			//this.dd.disable();
			classie.add( this.el, 'show-content' );	
			classie.add( page, 'show' );
		}

		var self = this,
			onEndTransitionFn = function( ev ) {
				if( support.transitions ) {
					if( ev.propertyName.indexOf( 'transform' ) === -1 || ev.target !== this ) return;
					this.removeEventListener( transEndEventName, onEndTransitionFn );
				}
				if( self.isContent ) {
					classie.remove( page, 'show' );	
				}
				self.isContent = !self.isContent;
				self.isAnimating = false;
				// callback
				self.options.onToggleContentComplete();
			};

		if( support.transitions ) {
			this.el.addEventListener( transEndEventName, onEndTransitionFn );
		}
		else {
			onEndTransitionFn();
		}
	}
    */
	/**
	 * initialize the Dragdealer plugin
	 */
    
	DragSlideshow.prototype._initDragDealer = function() {
		var self = this;
		$(".handle").dragend({
            
            onDragStart: function(){
                //self.isAnimating = true;
                //$("#pointMenu .point").removeClass("active");
            },
            onDragEnd: function(){
                //self.isAnimating = false;
                $(".slide").removeClass("current");
                self.current = this.page;
                $(this.pages[ this.page ]).addClass("current");
                $("#numSlider").val(this.page).change();
                
            },
            onSwipeEnd: function(){
                self.isAnimating = false;
                self.current = this.page;
                $("#container").css("background-color",$(this.pages[ this.page ]).attr("data-colore"));
                
                //self.currentSlug = $("")
                self.currentSlug = $(self.slides[self.current]).data("slug");
                

                //self.autoOpen = false;
                
                $("#pointMenu .point[data-num='"+this.page+"']").addClass("active");
                
                if(self.currentSlug=="home") {
                    $(".mn1 a").css("opacity","1");
                    $(".mn2 a").css("opacity","0.5");
                    $(".mn3 a").css("opacity","0.5");
                    self.hideContatti(); //forse
                } else if(self.currentSlug=="contatti") {
                    $(".mn1 a").css("opacity","0.5");
                    $(".mn2 a").css("opacity","0.5");
                    $(".mn3 a").css("opacity","1");  
                    self.showContatti();
                }else{
                    $(".mn1 a").css("opacity","0.5");
                    $(".mn2 a").css("opacity","1");
                    $(".mn3 a").css("opacity","0.5");                    
                    self.hideContatti(); // forse
                    //history.pushState(null,self.currentSlug,$("#homeUrl").val() + "/portfolio-" + self.currentSlug);
                }
                
                if(self.autoscrollTo == true && self.currentSlug=="home")
                {
                    self.autoscrollTo = false;
                    //$(".slide-home").trigger("click")
                    
                }
                self.autoscrollTo = false;
                
                if(self.autoOpen ||  self.currentSlug=="home"){
                    //alert("autoOpenato");
                    self.autoOpen = false;
                    //window.setTimeout(self.toggle(),f, 2000);
                    //window.setTimeout($(".slide-home").trigger("click"),null, 8000);
                    setTimeout(function(){ self.toggle(); }, 100);
                    history.pushState(null,self.currentSlug,$("#homeUrl").val() + "/"+self.currentSlug);
                }       
                
                if(self.currentSlug=="home") {
                    history.pushState(null,self.currentSlug,$("#homeUrl").val() + "/home");
                }
                
            },
            onSwipeStart: function(){
                $("#pointMenu .point").removeClass("active");
                self.isAnimating = true;
                
            },
            afterInitialize: function(){
                self._initTitlesPosition();
                $("#container").css("visibility","visible");
                this.jumpToPage( self.current+1 );
            }
            
        });
	}

	/**
	 * DragDealer plugin callback: update current value
	 */
	DragSlideshow.prototype._navigate = function( x, y ) {
		// add class "current" to the current slide / remove that same class from the old current slide
		classie.remove( this.slides[ this.current || 0 ], 'current' );
		this.current = this.dd.getStep()[0] - 1;
		classie.add( this.slides[ this.current ], 'current' );
		//alert("navigato");
					var s = this.slides;
					var c = this.current;
					this.slides.forEach( function( slide ) {
				if(s.indexOf( slide ) !== c){
					$(slide).css("transform","scale(.95,.8)");
				} else {
					$(slide).css("transform","scale(1)");
				}
			} );		

	}

    function autoscrollTo(pg) {

        if( self.isFullscreen || self.isAnimating ) return false;
        $(".slide").removeClass("current");

        self.current = pg;
        $(".slide[data-index="+self.current+"]").addClass("current");
        
        $(".handle").dragend({
                scrollToPage: (parseInt(pg)+1)
            });
        $("#pointMenu .point").removeClass("active");
        $("#pointMenu .point[data-num='"+pg+"']").addClass("active");
    }
    
	/**
	 * toggle between fullscreen and minimized slideshow
	 */
	DragSlideshow.prototype.toggle = function(autoscroll,page) {
	
		if(autoscroll === undefined){
		    autoscroll = false;
		}

		if(page === undefined){
		    page = 0;
		}	

		if( this.isAnimating ) {
			return false;
		}

        //this.autoOpen = false;
        //$("body").toggleClass("full");
        
        $(".div-arrow").removeClass("bounce");
        
		this.isAnimating = true;

		// add preserve-3d to the slides (seems to fix a rendering problem in firefox)
		this._preserve3dSlides( true );
		
		// callback
		this.options.onToggle();

		classie.remove( this.el, this.isFullscreen ? 'switch-max' : 'switch-min' );
		classie.add( this.el, this.isFullscreen ? 'switch-min' : 'switch-max' );
		
		var self = this,
			p = this.options.perspective,
			r = this.options.slideshowRatio,
			zAxisVal = this.isFullscreen ? p - ( p / r ) : p - p * r;

        
		if(this.isFullscreen){
            $("#bgvid").trigger('pause');
            
            //$( window).scrollTop( 0 );
            self.scrollingTop = true;
            var v = 300;

            if($(document).scrollTop()<10){
                v=0;
            }
            $(".slide-"+self.currentSlug+" .img_blurred").velocity({opacity:0},300);
            $("html,body").animate({scrollTop: $(document).scrollTop()-100},300,function(){

            });    
            $(".pages").animate({opacity: 0},v,function(){
                $(".pages").css("opacity",1);
$("#header").toggle().toggle();                
                self.imgDragger.style.WebkitTransform = 'perspective(' + self.options.perspective + 'px) translate3d( 0, 50%, ' + zAxisVal + 'px )';
			    self.imgDragger.style.transform = 'perspective(' + self.options.perspective + 'px) translate3d( 0, 50%, ' + zAxisVal + 'px )';    
                $("#header").removeClass("outed");
                $("body").removeClass("full");
                $("body").removeClass("allFull");  
$("#header").toggle().toggle();
            });
                 
                    
                //self.scrollingTop = false;
            
            
            $(".slider-switch").velocity({top:"-60px"},200);
            
			//self.imgDragger.style.WebkitTransform = 'perspective(' + self.options.perspective + 'px) translate3d( 0, 50%, ' + zAxisVal + 'px )';
            //self.imgDragger.style.transform = 'perspective(' + self.options.perspective + 'px) translate3d( 0, 50%, ' + zAxisVal + 'px )';    
            
			
		} else {
			
            /*
			this.slides.forEach( function( slide ) {
				//if(self.slides.indexOf( slide ) !== self.current){
					//$(slide).css("transform","scale(1)");
					//$(self.slides[self.current]).css("border-radius","30px");
				//}
			});
            */
			// ingrandisco
            $("body").addClass("full");
            $("#header").addClass("outed");
            $(".page-intro").css('opacity',0);
$("#header").toggle().toggle();
            $(".slide-"+self.currentSlug+" .img_blurred").velocity({opacity:1},300);
			this.imgDragger.style.WebkitTransform = 'perspective(' + this.options.perspective + 'px) translate3d( 0, 0, 0 )';
			this.imgDragger.style.transform = 'perspective(' + this.options.perspective + 'px) translate3d( 0, 0, 0 )';
$("#header").toggle().toggle();
            self._loadContentPage();
            
            if(self.current==0)
                $("#bgvid").trigger('play');
            else
                $("#bgvid").trigger('pause');
            
		}


		var onEndTransitionFn = function( ev ) {
				
			if( support.transitions ) {
				if( ev.propertyName.indexOf( 'transform' ) === -1 ) return;
				this.removeEventListener( transEndEventName, onEndTransitionFn );
			}

			if( !self.isFullscreen ) {
                //ingrandisco
				// remove preserve-3d to the slides (seems to fix a rendering problem in firefox)
				//alert("fine apertura");
				self._preserve3dSlides();
			     
                $(".page-intro").velocity({"opacity":1},200);	
				$("body").addClass("allFull");
                $(".slider-switch").velocity({top:self.menuOffset + "px"},200);
				
				//(self.slides[self.current]).style.WebkitTransform = 'none';
				//(self.slides[self.current]).style.transform = 'none';
				//$(self.slides[self.current]).css("border-radius","0");

			} else {

				//$(self.slides[self.current]).css("border-radius","30px");				
				//(self.slides[self.current]).style.WebkitTransform = 'none';
				//(self.slides[self.current]).style.transform = 'none';
                
                //distruggo e ricreo i video yt per stopparli.
                //$("#content-"+self.currentSlug+" iframe").each(function(){
                //    $(this).attr("src","about:blank");
                //});
                $("iframe").each(function(){
                    $(this).attr("src","about:blank");
                });
                $("#content-"+self.currentSlug+" .rwd-video").each(function(){
                    $(this).removeClass("play");
                    $(this).addClass("stop");
                    //$(this).find("a").css("display:inline-block");
                    //$(this).find("img").css("display:inline-block");
                });
                

			}

			// replace class "img-dragger-large" with "img-dragger-small"
			classie.remove( this, self.isFullscreen ? 'img-dragger-large' : 'img-dragger-small' );
			classie.add( this, self.isFullscreen ? 'img-dragger-small' : 'img-dragger-large' );

			// reset transforms and set width & height
			//this.style.width = self.isFullscreen ? self.options.slideshowRatio * 100 + '%' : '100%';
			//this.style.height = self.isFullscreen ? self.options.slideshowRatio * 100 + '%' : '100%';
			// reinstatiate the dragger with the "reflow" method

			// change status
			self.isFullscreen = !self.isFullscreen;
			
			self.isAnimating = false;
            self.scrollingTop = false;
            if(autoscroll) {
                self.autoscrollTo = true;
                autoscrollTo(page);
                //alert("ho scrollato3");
                //self.autoOpen = true;
            }
		};

		if( support.transitions ) {
			this.imgDragger.addEventListener( transEndEventName, onEndTransitionFn );
		}
		else {
			onEndTransitionFn();
		}
	}

	/**
	 * add/remove preserve-3d to the slides (seems to fix a rendering problem in firefox)
	 */
	DragSlideshow.prototype._preserve3dSlides = function( add ) {
		this.slides.forEach( function( slide ) {
			slide.style.transformStyle = add ? 'preserve-3d' : '';
		});
	}

	/**
	 * add to global namespace
	 */
	window.DragSlideshow = DragSlideshow;

} )( window );